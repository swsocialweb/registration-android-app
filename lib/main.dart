import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Form Demo',
      theme: new ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: new MyHomePage(title: 'Registro Empleados Recursos Humanos'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _colors = <String>['', 'Secretaría de Innovación', 'Secretaría Jurídica', 'Secretaría de Comercio', 'Recursos Humanos'];
  String _color = '';

  List<String> _genders = <String>['', 'Masculino', 'Femenino', 'Prefiero no decir'];
  String _gender = '';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.person),
                      hintText: 'Ingrese su nombre completo',
                      labelText: 'Nombre completo',
                    ),
                  ),



                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.calendar_today),
                      hintText: 'Ingrese su fecha de nacimiento',
                      labelText: 'Fecha de nacimiento',
                    ),
                    keyboardType: TextInputType.datetime,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.phone),
                      hintText: 'Ingrese su número de teléfono',
                      labelText: 'Teléfono',
                    ),
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.email),
                      hintText: 'Ingrese su correo electrónico',
                      labelText: 'Correo electrónico',
                    ),
                    keyboardType: TextInputType.emailAddress,
                  ),

                  new FormField(
                      builder: (FormFieldState state)
                          {
                            return InputDecorator(
                              decoration: InputDecoration(
                                icon: const Icon(Icons.person),
                                labelText: 'Género',
                              ),
                              isEmpty: _gender == '',
                              child: new DropdownButtonHideUnderline(
                                  child: new DropdownButton(
                                      value: _gender,
                                      isDense: true,
                                      onChanged: (String newValue){
                                        setState(() {
                                          _gender = newValue;
                                          state.didChange(newValue);
                                        });
                                      },
                                    items: _genders.map((String value) {
                                      return new DropdownMenuItem(
                                        value: value,
                                        child: new Text(value),
                                      );
                                    }).toList(),
                                  ),
                              ),
                            );

                          },
                  ),

                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.my_location),
                          labelText: 'Área a la que pertenece',
                        ),
                        isEmpty: _color == '',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _color,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                //newContact.favoriteColor = newValue;
                                _color = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _colors.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),

                  new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.account_box),
                        hintText: 'Ingrese subárea',
                        labelText: 'Subárea',
                      )
                  ),

                  new Container(
                      padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Submit'),
                        onPressed: null,
                      )),
                ],
              ))),
    );
  }
}